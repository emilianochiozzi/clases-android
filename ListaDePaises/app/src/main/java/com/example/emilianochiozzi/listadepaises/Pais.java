package com.example.emilianochiozzi.listadepaises;

/**
 * Created by emilianochiozzi on 10/11/2017.
 */

public class Pais {

    public String nombre;
    public int bandera;

    public Pais() {
        super();
    }

    public Pais(String nombre, int bandera) {
        super();
        this.nombre = nombre;
        this.bandera = bandera;
    }

}
