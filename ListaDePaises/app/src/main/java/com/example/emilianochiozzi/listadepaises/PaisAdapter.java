package com.example.emilianochiozzi.listadepaises;

import android.app.Activity;
import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.ImageView;
import android.widget.TextView;

/**
 * Created by emilianochiozzi on 10/11/2017.
 */

public class PaisAdapter extends ArrayAdapter<Pais> {

    Context myContext;
    int myLayoutResourceID;
    Pais[] myData = null;


    public PaisAdapter(Context context, int layoutResourceID,Pais[] data) {
        super(context,layoutResourceID,data);

        this.myContext = context;
        this.myLayoutResourceID = layoutResourceID;
        this.myData = data;
    }

    public View getView(int position, View convertView, ViewGroup parent) {

        View row = convertView;

        PaisHolder holder = null;

        if (row == null) {
            LayoutInflater inflater = ((Activity)myContext).getLayoutInflater();
            row = inflater.inflate(myLayoutResourceID,parent,false);
        }

        holder = new PaisHolder();
        holder.bandera = (ImageView)row.findViewById(R.id.bandera);
        holder.nombre = (TextView)row.findViewById(R.id.nombre);

        Pais pais = myData[position];
        holder.nombre.setText(pais.nombre);
        holder.bandera.setImageResource(pais.bandera);

        return row;
    }

    static class PaisHolder {
        ImageView bandera;
        TextView nombre;
    }
}
