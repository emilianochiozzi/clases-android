package com.example.emilianochiozzi.listadepaises;

import android.os.AsyncTask;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.util.JsonReader;
import android.util.Log;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ListView;
import android.widget.Toast;

import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.net.URL;
import java.util.ArrayList;
import java.util.List;

import javax.net.ssl.HttpsURLConnection;

public class MainActivity extends AppCompatActivity {

    ListView listaPaises;

    public List<Producto> results;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        String[] paises = new String[] {
                "Argentina",
                "Brasil",
                "Uruguay",
                "Paraguay",
                "Bolivia",
                "Chile",
                "Colombia",
                "Ecuador",
                "Venezuela",
                "Peru",
                "Italia",
                "Francia",
                "Alemania"
        };

        //ArrayAdapter<String> adapter = new ArrayAdapter<String>(this,android.R.layout.simple_expandable_list_item_1,paises);

        final Pais[] paises_datos = new Pais[] {
                new Pais("Argentina",R.drawable.ar),
                new Pais("Brasil",R.drawable.br),
                new Pais("Uruguay",R.drawable.uy),
                new Pais("Paraguay",R.drawable.py),
                new Pais("Bolivia",R.drawable.bo),
                new Pais("Chile",R.drawable.cl),
                new Pais("Colombia",R.drawable.co),
                new Pais("Ecuador",R.drawable.ec),
                new Pais("Venezuela",R.drawable.ve),
                new Pais("Peru",R.drawable.pe),
                new Pais("Italia",R.drawable.it),
                new Pais("Francia",R.drawable.fr),
                new Pais("Alemania",R.drawable.de)
        };


        listaPaises = (ListView)findViewById(R.id.listaPaises);

        PaisAdapter adapter  = new PaisAdapter(this,R.layout.celda_pais,paises_datos);

        listaPaises.setAdapter(adapter);

        listaPaises.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> adapterView, View view, int i, long l) {

                Toast.makeText(getApplicationContext(),paises_datos[i].nombre,Toast.LENGTH_SHORT).show();
            }
        });

        AsyncTask.execute(new Runnable() {
            @Override
            public void run() {
                try {
                    URL mlEndpoint = new URL("https://api.mercadolibre.com/sites/MLU/search?q=chromecast");
                    HttpsURLConnection myConnection = (HttpsURLConnection) mlEndpoint.openConnection();

                    if (myConnection.getResponseCode() == 200) {
                        InputStream responseBody = myConnection.getInputStream();
                        InputStreamReader responseBodyReader = new InputStreamReader(responseBody, "UTF-8");
                        JsonReader jsonReader = new JsonReader(responseBodyReader);

                        jsonReader.beginObject();
                        while (jsonReader.hasNext()) {
                            String key = jsonReader.nextName();
                            if (key.equals("results")) {
                                results = readProductosArray(jsonReader);
                                Log.d("Title",results.get(0).title);
                                Log.d("Title",results.get(10).title);
                            } else {
                                jsonReader.skipValue(); // Skip values of other keys
                            }
                        }
                        jsonReader.close();
                        myConnection.disconnect();
                    } else {
                        Log.d("Error:","Error-----");
                    }
                } catch (Exception e) { // The object "e" is the exception object that was thrown.
                    // this is where you handle it if an error occurs
                    Log.d("Exception:","Exception-----");
                }
            }
        });

    }

    public List<Producto> readProductosArray(JsonReader reader) throws IOException {
        List<Producto> messages = new ArrayList<Producto>();

        reader.beginArray();
        while (reader.hasNext()) {
            messages.add(readProducto(reader));
        }
        reader.endArray();
        return messages;
    }

    public Producto readProducto(JsonReader reader) throws IOException {

        String title = null;
        Double price = null;


        reader.beginObject();
        while (reader.hasNext()) {
            String name = reader.nextName();
            if (name.equals("title")) {
                title = reader.nextString();
            } else if (name.equals("price")) {
                price = reader.nextDouble();
            } else {
                reader.skipValue();
            }
        }
        reader.endObject();
        return new Producto(title, price);
    }
}
