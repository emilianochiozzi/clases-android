package com.example.emilianochiozzi.listadepaises;

/**
 * Created by emilianochiozzi on 20/11/2017.
 */

public class Producto {

    public String title;
    public double price;

    public Producto() {
        super();
    }

    public Producto(String title, double price) {
        super();
        this.title = title;
        this.price = price;
    }
}
