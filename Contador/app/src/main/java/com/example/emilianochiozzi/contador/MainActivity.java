package com.example.emilianochiozzi.contador;

import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.view.View;
import android.widget.TextView;

public class MainActivity extends AppCompatActivity {

    public int contador;

    @Override
    protected void onCreate(Bundle savedInstanceState) {

        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        contador = 0;
        mostrarResultado();
    }

    public void incrementaContador(View vista) {
        contador++;
        mostrarResultado();
    }

    public void decrementaContador(View vista) {
        contador--;
        mostrarResultado();
    }

    public void reseteaContador(View vista) {
        contador = 0;
        mostrarResultado();
    }

    public void mostrarResultado() {
        TextView resultado = (TextView)findViewById(R.id.textViewResultado);

        resultado.setText("Contador: " + contador);
    }


}
