package com.example.emilianochiozzi.pantallas;

import android.content.Intent;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.view.View;

public class MainActivity extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
    }


    public void irASegundaPantalla(View vista) {

        Intent i = new Intent(this,SegundaPantalla.class);
        //agregar parametros a la intencion
        i.putExtra("texto","Hola desde la primera");
        startActivity(i);

    }
}
