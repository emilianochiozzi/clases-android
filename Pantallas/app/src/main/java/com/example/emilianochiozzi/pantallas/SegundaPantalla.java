package com.example.emilianochiozzi.pantallas;

import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.widget.TextView;

/**
 * Created by emilianochiozzi on 10/11/2017.
 */

public class SegundaPantalla extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.pantalla_segunda);

        //lee los parametros de la intencion
        Bundle datos = getIntent().getExtras();
        //guarda un parametro en particular
        String texto = datos.getString("texto");

        //accedo a una view
        TextView textoSegundaPantalla = (TextView)findViewById(R.id.textViewSegundaPantalla);
        textoSegundaPantalla.setText(texto);
    }

    @Override
    public void onBackPressed() {
        super.onBackPressed();

        moveTaskToBack(true);
    }
}
