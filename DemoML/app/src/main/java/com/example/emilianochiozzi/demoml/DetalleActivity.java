package com.example.emilianochiozzi.demoml;

import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;

public class DetalleActivity extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_detalle);

        //lee los parametros de la intencion
        Bundle datos = getIntent().getExtras();
        //guarda un parametro en particular
        String titulo = datos.getString("titulo");
    }
}
