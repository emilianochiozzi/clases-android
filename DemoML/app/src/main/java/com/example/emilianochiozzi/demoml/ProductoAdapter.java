package com.example.emilianochiozzi.demoml;

import android.app.Activity;
import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.ImageView;
import android.widget.TextView;

import com.squareup.picasso.Picasso;

import java.util.List;

/**
 * Created by emilianochiozzi on 20/11/2017.
 */


public class ProductoAdapter extends ArrayAdapter<Producto> {

    Context myContext;
    int myLayoutResourceID;
    List<Producto> myData = null;


    public ProductoAdapter(Context context, int layoutResourceID,List<Producto> data) {
        super(context,layoutResourceID,data);

        this.myContext = context;
        this.myLayoutResourceID = layoutResourceID;
        this.myData = data;
    }

    public View getView(int position, View convertView, ViewGroup parent) {

        View row = convertView;

        ProductoHolder holder = null;

        if (row == null) {
            LayoutInflater inflater = ((Activity)myContext).getLayoutInflater();
            row = inflater.inflate(myLayoutResourceID,parent,false);
        }

        holder = new ProductoHolder();
        holder.image = (ImageView)row.findViewById(R.id.image);
        holder.title = (TextView)row.findViewById(R.id.title);
        holder.price = (TextView)row.findViewById(R.id.price);

        Producto producto = myData.get(position);
        holder.title.setText(producto.title);
        holder.price.setText("$" + String.valueOf(producto.price));
        Picasso.with(getContext()).load(producto.image).fit().into(holder.image);

        return row;
    }

    static class ProductoHolder {
        ImageView image;
        TextView title;
        TextView price;
    }
}

