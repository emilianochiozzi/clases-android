package com.example.emilianochiozzi.demoml;

/**
 * Created by emilianochiozzi on 20/11/2017.
 */

public class Producto {

    public String title;
    public double price;
    public String image;

    public Producto() {
        super();
    }

    public Producto(String title, double price, String image) {
        super();
        this.title = title;
        this.price = price;
        this.image = image;
    }
}
