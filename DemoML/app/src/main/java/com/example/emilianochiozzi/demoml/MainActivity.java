package com.example.emilianochiozzi.demoml;

import android.content.Intent;
import android.os.AsyncTask;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.util.JsonReader;
import android.util.Log;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ListView;
import android.widget.SearchView;

import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.net.URL;
import java.util.ArrayList;
import java.util.List;

import javax.net.ssl.HttpsURLConnection;

public class MainActivity extends AppCompatActivity {

    ListView listaProductos;

    public List<Producto> results;

    public String textoBusqueda;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        listaProductos = (ListView)findViewById(R.id.listaProductos);

        listaProductos.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> adapterView, View view, int i, long l) {

                Intent intencion = new Intent(MainActivity.this,DetalleActivity.class);
                //agregar parametros a la intencion
                intencion.putExtra("titulo",results.get(i).title);
                startActivity(intencion);
            }
        });

    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {

        MenuInflater inflater = getMenuInflater();
        inflater.inflate(R.menu.menu_search, menu);

        MenuItem item = menu.findItem(R.id.menuSearch);

        SearchView searchView = (SearchView)item.getActionView();
        searchView.setOnQueryTextListener(new SearchView.OnQueryTextListener() {
            @Override
            public boolean onQueryTextSubmit(String query) {
                textoBusqueda = query;
                llamarApiMercadoLibre();
                return false;
            }

            @Override
            public boolean onQueryTextChange(String newText) {
                return false;
            }
        });

        return super.onCreateOptionsMenu(menu);

    }



    public void llamarApiMercadoLibre() {
        AsyncTask.execute(new Runnable() {
            @Override
            public void run() {
                try {
                    URL mlEndpoint = new URL("https://api.mercadolibre.com/sites/MLA/search?q=" + textoBusqueda);
                    HttpsURLConnection myConnection = (HttpsURLConnection) mlEndpoint.openConnection();

                    if (myConnection.getResponseCode() == 200) {
                        InputStream responseBody = myConnection.getInputStream();
                        InputStreamReader responseBodyReader = new InputStreamReader(responseBody, "UTF-8");
                        JsonReader jsonReader = new JsonReader(responseBodyReader);

                        jsonReader.beginObject();
                        while (jsonReader.hasNext()) {
                            String key = jsonReader.nextName();
                            if (key.equals("results")) {
                                results = armarProductosArray(jsonReader);
                            } else {
                                jsonReader.skipValue();
                            }
                        }
                        jsonReader.close();
                        myConnection.disconnect();
                    }
                } catch (Exception e) {
                    Log.d("Exception:","Exception");
                }
                runOnUiThread(new Runnable() {
                    @Override
                    public void run() {
                        ProductoAdapter adapter  = new ProductoAdapter(MainActivity.this,R.layout.celda_producto,results);
                        listaProductos.setAdapter(adapter);
                    }
                });

            }
        });
    }

    public List<Producto> armarProductosArray(JsonReader reader) throws IOException {
        List<Producto> productos = new ArrayList<Producto>();

        reader.beginArray();
        while (reader.hasNext()) {
            productos.add(leerProducto(reader));
        }
        reader.endArray();
        return productos;
    }

    public Producto leerProducto(JsonReader reader) throws IOException {

        String title = null;
        Double price = null;
        String image = null;

        reader.beginObject();
        while (reader.hasNext()) {
            String name = reader.nextName();
            if (name.equals("title")) {
                title = reader.nextString();
            } else if (name.equals("price")) {
                price = reader.nextDouble();
            } else if (name.equals("thumbnail")) {
                image = reader.nextString();
            } else {
                reader.skipValue();
            }
        }
        reader.endObject();
        return new Producto(title, price, image);
    }
}

